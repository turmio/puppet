# Install procmail
#
class procmail {

    package { "procmail":
        ensure => present,
    }

    file { "/etc/procmailrc.d":
        ensure  => directory,
        purge   => true,
        force   => true,
        recurse => true,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source  => "puppet:///modules/custom/empty",
        require => Package["procmail"],
        notify  => Exec["generate-procmailrc"],
    }

    file { "/etc/procmailrc":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => Package["procmail"],
    }

    exec { "generate-procmailrc":
        command     => '/bin/sh -c -- \'umask 022; ( echo "DROPPRIVS=yes" ; find /etc/procmailrc.d/*.rc -exec echo "INCLUDERC={}" \; ) > /etc/procmailrc ; true\'',
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
    }

}


# Add config file to procmail
#
# === Parameters
#
#   $name:
#       Config file name.
#   $source:
#       Config file source. Defaults to "puppet:///files/procmail/$name".
#   $content:
#       Config file content.
#
# === Sample usage
#
# procmail::rc { "spamassassin.rc":
#     source => "/etc/mail/spamassassin/spamassassin-spamc.rc",
# }
#
define procmail::rc($source="AUTO", $content=undef) {

    if $content {
        $source_real = undef
    } else {
        $source_real = $source ? {
            "AUTO"  => "puppet:///files/procmail/${name}",
            default => $source,
        }
    }

    file { "/etc/procmailrc.d/${name}":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        content => $content,
        source  => $source_real,
        notify  => Exec["generate-procmailrc"],
    }

}
