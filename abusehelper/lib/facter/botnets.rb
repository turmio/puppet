Facter.add(:botnets) do
    setcode do
        botnets = []
        if File.executable?("/usr/bin/botnet")
            listcmd = "su -s /bin/sh - nobody -c '/usr/bin/botnet list'"
            output = Facter::Util::Resolution.exec(listcmd)
            output.lines.each do |line|
                if m = line.match(/^\[\d+\]\s+(.*\.py)/)
                    botnets << File.basename(File.dirname(m[1]))
                end
            end
        end
        botnets.sort.join(',')
    end
end
