# Install Sendmail packages.
#
class sendmail::common {

    if !$mail_domain and $domain {
        $mail_domain = $domain
    }

    if $::operatingsystem != "OpenBSD" {
        package { "sendmail":
            ensure => installed,
            name   => [ "sendmail", "sendmail-cf", ],
            before => File["/etc/mail/certs"],
        }
        service { "sendmail":
            ensure  => running,
            enable  => true,
            require => Package["sendmail"],
        }
    }

    case $::operatingsystem {
        "centos","redhat","fedora": {
            file { "/etc/sysconfig/sendmail":
                ensure  => present,
                content => "DAEMON=no\nQUEUE=1h\n",
                owner   => "root",
                group   => "root",
                mode    => "0644",
                notify  => Service["sendmail"],
            }
            $cabundle = "/etc/pki/tls/certs/ca-bundle.crt"
        }
        "ubuntu","debian": {
            $cabundle = "/etc/ssl/certs/ca-certificates.crt"
        }
        "openbsd": {
            $cabundle = "/etc/ssl/cert.pem"
        }
        default: {
            fail("sendmail module not supported on ${::operatingsystem}")
        }
    }

    file { "/etc/mail/certs":
        ensure  => directory,
        mode    => "0755",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }
    exec { "populate-etc-mail-certs":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => "csplit -k ${cabundle} '/BEGIN/' '{1000}' ; sh -c 'for i in x* ; do name=`openssl x509 -hash -noout -in \$i`.0 ; openssl x509 -hash -in \$i -out \$name ; done' && rm -f x* .0",
        cwd     => "/etc/mail/certs",
        onlyif  => "find /etc/mail/certs ! -newer ${cabundle} | egrep '.*' || [ -z \"`ls /etc/mail/certs`\" ]",
        require => File["/etc/mail/certs"],
    }

}


# Configure Sendmail submission.
#
# === Global variables
#
#   $mail_server:
#       Hostname of mail server.
#
class sendmail::client inherits sendmail::common {

    if !$mail_server {
        $mail_server = "127.0.0.1"
    }

    file { "/etc/mail/submit.mc":
        path    => $::operatingsystem ? {
            "openbsd" => "/usr/share/sendmail/cf/submit.mc",
            default   => "/etc/mail/submit.mc",
        },
        ensure  => present,
        content => template("sendmail/submit.mc.erb", "sendmail/submit.mc.msp.erb"),
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "bin",
            default   => "root",
        },
        mode    => "0644",
        notify  => Exec["make submit.cf"],
        require => $::operatingsystem ? {
            "openbsd" => undef,
            default   => Package["sendmail"],
        },
    }

    exec { "make submit.cf":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin:/etc/mail",
        cwd         => $::operatingsystem ? {
            "openbsd" => "/usr/share/sendmail/cf",
            default   => "/etc/mail",
        },
        refreshonly => true,
        notify      => $::operatingsystem ? {
            "openbsd" => undef,
            default   => Service["sendmail"],
        },
    }

    file { "/etc/mail/submit.cf":
        ensure  => present,
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            "ubuntu"  => "smmsp",
            default   => "root",
        },
        mode    => "0644",
        source  => $::operatingsystem ? {
            "openbsd" => "/usr/share/sendmail/cf/submit.cf",
            default   => undef,
        },
        require => Exec["make submit.cf"],
    }

    case $::operatingsystem {
        "centos","redhat","fedora": {
            case $mail_server {
                "127.0.0.1","localhost": {
                    File["/etc/sysconfig/sendmail"] {
                        content => "DAEMON=yes\nQUEUE=1h\n",
                    }
                }
            }
        }
    }

}


# Configure Sendmail submission using port 587.
#
class sendmail::client::msa inherits sendmail::client {

    File["/etc/mail/submit.mc"] {
        content => template("sendmail/submit.mc.erb", "sendmail/submit.mc.msa.erb"),
    }

}


# Configure Sendmail server.
#
#   $mail_domain:
#       SMTP domain name.
#
#   $mail_smart_host:
#       Deliver all mail to host defined. Add leading dot.
#       $mail_smart_host = "mail.example.com."
#
#   $masquerade_exception:
#       Everything is masqueraded to $mail_domain except these hosts.
#
#   $sendmail_auth_mech:
#       List of authentication mechanisms to use. Defaults to PLAIN and LOGIN.
#
#   $sendmail_ssl_key:
#       Path to SSL key file. If not set sendmail will be configured
#       without SSL support.
#
#   $sendmail_ssl_cert:
#       Path to SSL certificate file. If not set sendmail will be
#       configured without SSL support.
#
#   $sendmail_ssl_chain:
#       Path to SSL certificate chain file.
#
#   $sendmail_datadir:
#       Directory for mail spool.
#
#   $sendmail_greetpause:
#       Default greet_pause delay in milliseconds. Disabled by default.
#
#   $sendmail_milter:
#       List of milters to use. For example:
#       $sendmail_milter = [
#           $spamassassin::milter::sendmail_milter,
#       ]
#
class sendmail::server inherits sendmail::common {

    include procmail

    if !$sendmail_auth_mech {
        $sendmail_auth_mech = [ "PLAIN", "LOGIN", ]
    }

    if !$masquerade_exception {
        $masquerade_exception = []
    }

    selinux::manage_fcontext { "/etc/smrsh(/.*)?":
        type => "sendmail_exec_t",
    }

    case $::operatingsystem {
        "centos","redhat","fedora": {
            File["/etc/sysconfig/sendmail"] {
                content => "DAEMON=yes\nQUEUE=1h\n",
            }
        }
        default: {
            fail("sendmail::server not supported in '${::operatingsystem}'")
        }
    }

    if $sendmail_datadir {
        file { $sendmail_datadir:
            ensure  => directory,
            mode    => "0775",
            owner   => "root",
            group   => "mail",
            seltype => "mail_spool_t",
        }
        selinux::manage_fcontext { "${sendmail_datadir}(/.*)?":
            type   => "mail_spool_t",
            before => File[$sendmail_datadir],
        }
        file { "/var/spool/mail":
            ensure  => link,
            target  => $sendmail_datadir,
            owner   => "root",
            group   => "mail",
            purge   => true,
            force   => true,
            require => File[$sendmail_datadir],
            before  => Service["sendmail"],
        }
    }

    if $sendmail_ssl_key and $sendmail_ssl_cert {
        $ssl_key = basename($sendmail_ssl_key)
        $ssl_cert = basename($sendmail_ssl_cert)
        if $sendmail_ssl_chain {
            $ssl_chain = basename($sendmail_ssl_chain)
        }

        include ssl

        file { "${ssl::private}/${ssl_key}":
            ensure => present,
            mode   => "0600",
            owner  => "root",
            group  => "root",
            source => $sendmail_ssl_key,
            notify => Service["sendmail"],
        }
        file { "${ssl::certs}/${ssl_cert}":
            ensure => present,
            mode   => "0644",
            owner  => "root",
            group  => "root",
            source => $sendmail_ssl_cert,
            notify => Service["sendmail"],
        }
        if $ssl_chain {
            file { "${ssl::certs}/${ssl_chain}":
                ensure => present,
                mode   => "0644",
                owner  => "root",
                group  => "root",
                source => $sendmail_ssl_chain,
                notify => Service["sendmail"],
            }
        }

        if $sendmail_auth_mech != [] {
            sasl::saslauthd::service { "Sendmail":
                content => template("sendmail/Sendmail.conf.erb"),
                notify  => Service["sendmail"],
            }
        }

    }

    file { "/etc/mail/sendmail.mc":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        content => template("sendmail/sendmail.mc.erb"),
        notify  => Exec["make sendmail.cf"],
        require => Package["sendmail"],
    }
    exec { "make sendmail.cf":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin:/etc/mail",
        cwd         => "/etc/mail",
        refreshonly => true,
        notify      => Service["sendmail"],
    }

    file { "/etc/aliases":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => [
            "puppet:///files/mail/aliases.${::homename}",
            "puppet:///files/mail/aliases",
            "puppet:///modules/sendmail/aliases",
        ],
        notify  => Exec["newaliases"],
        require => Package["sendmail"],
    }
    exec { "newaliases":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
    }

    file { "/etc/mail/access":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => [
            "puppet:///files/mail/access.${::homename}",
            "puppet:///files/mail/access",
            "puppet:///modules/sendmail/empty",
        ],
        notify  => Exec["make access.db"],
        require => Package["sendmail"],
    }
    exec { "make access.db":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => "makemap hash /etc/mail/access < /etc/mail/access",
        refreshonly => true,
        notify      => Service["sendmail"],
    }

    file { "/etc/mail/genericstable":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => [
            "puppet:///files/mail/genericstable.${::homename}",
            "puppet:///files/mail/genericstable",
            "puppet:///modules/sendmail/empty",
        ],
        notify  => Exec["make genericstable.db"],
        require => Package["sendmail"],
    }
    exec { "make genericstable.db":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => "makemap hash /etc/mail/genericstable < /etc/mail/genericstable",
        refreshonly => true,
        notify      => Service["sendmail"],
    }

    file { "/etc/mail/mailertable":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source => [
            "puppet:///files/mail/mailertable.${::homename}",
            "puppet:///files/mail/mailertable",
            "puppet:///modules/sendmail/empty",
        ],
        notify  => Exec["make mailertable.db"],
        require => Package["sendmail"],
    }
    exec { "make mailertable.db":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => "makemap hash /etc/mail/mailertable < /etc/mail/mailertable",
        refreshonly => true,
        notify      => Service["sendmail"],
    }

    file { "/etc/mail/virtusertable":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => [
            "puppet:///files/mail/virtusertable.${::homename}",
            "puppet:///files/mail/virtusertable",
            "puppet:///modules/sendmail/empty",
        ],
        notify  => Exec["make virtusertable.db"],
        require => Package["sendmail"],
    }
    exec { "make virtusertable.db":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => "makemap hash /etc/mail/virtusertable < /etc/mail/virtusertable",
        refreshonly => true,
        notify      => Service["sendmail"],
    }

    file { "/etc/mail/local-host-names":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => [
            "puppet:///files/mail/local-host-names.${::homename}",
            "puppet:///files/mail/local-host-names",
            "puppet:///modules/sendmail/empty",
        ],
        notify  => Service["sendmail"],
        require => Package["sendmail"],
    }

}
