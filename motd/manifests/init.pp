
# Deploy motd file to server
#
class motd {

    case $::operatingsystem {
        "ubuntu": {
            package { "update-motd":
                ensure => absent,
            }
        }
    }

    file { "/etc/motd":
        ensure => present,
        source => [
          "puppet:///files/motd/motd.${::homename}",
          "puppet:///files/motd/motd",
          "puppet:///modules/motd/empty",
        ],
        mode   => "0644",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }

}
