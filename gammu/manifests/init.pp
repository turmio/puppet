
# Install Gammu packages
#
class gammu {

    package { "gammu":
        ensure => installed,
    }

}


# Install Gammu SMS daemon
#
# === Parameters
#
#     $port:
#         Serial port where modem is located.
#     $pin:
#         PIN code for SIM card, defaults to no PIN.
#     $receivecmd:
#         Source for command to run for received messages. Defaults to
#         "gammu-smsd-receive" script from module.
#
class gammu::smsd($port,
                  $pin=undef,
                  $receivecmd="puppet:///modules/gammu/gammu-smsd-receive") {

    require gammu

    user { "smsd":
        ensure  => present,
        comment => "Service SMS",
        gid     => "smsd",
        groups  => "dialout",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        system  => true,
        require => Group["smsd"],
    }
    group { "smsd":
        ensure => present,
        system => true,
    }

    file { "/var/spool/smsd":
        ensure  => directory,
        mode    => "0770",
        owner   => "smsd",
        group   => "smsd",
        require => User["smsd"],
    }

    if $receivecmd {
        file { "/usr/local/sbin/gammu-smsd-receive":
            ensure => present,
            source => $receivecmd,
            mode   => "0755",
            owner  => "root",
            group  => "root",
            before => Service["smsd"],
        }
    }

    file { "/etc/gammu-smsdrc":
        ensure  => present,
        content => template("gammu/gammu-smsdrc.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "smsd",
        require => Group["smsd"],
        notify  => Service["smsd"],
    }

    file { "/etc/init.d/smsd":
        ensure => present,
        source => "puppet:///modules/gammu/smsd.init",
        mode   => "0755",
        owner  => "root",
        group  => "root",
        notify => [ Exec["chkconfig --add smsd"], Service["smsd"], ],
    }
    exec { "chkconfig --add smsd":
        user        => "root",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
        require     => File["/etc/init.d/smsd"],
        before      => Service["smsd"],
    }
    service { "smsd":
        ensure    => running,
        enable    => true,
        hasstatus => true,
    }

}

