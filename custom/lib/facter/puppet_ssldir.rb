require 'puppet'

Facter.add('puppet_ssldir') do
    setcode do
        if Facter.value('puppetversion').to_i < 3
            Puppet.parse_config
        end
        Puppet.settings.value('ssldir')
    end
end
