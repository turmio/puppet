# Configure smtpd for local delivery.
#
# === Global variables
#
#   $mail_domain:
#       Domain to masquerade as (envelope only).
#
#   $mail_server:
#       Hostname of mail relay server.
#
class smtpd {

    if $mail_server {
        $relay = "smtp+tls://${mail_server}"
    }

    file { "/etc/mailer.conf":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "wheel",
        source => "puppet:///modules/smtpd/mailer.conf",
        before => Service["smtpd"],
    }

    file { "/etc/mail/smtpd.conf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "wheel",
        source  => "puppet:///modules/smtpd/smtpd.conf",
        notify  => Service["smtpd"],
    }

    file { "/etc/mail/smtpd.conf.local":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "wheel",
        content => template("smtpd/client.conf.erb"),
        notify  => Service["smtpd"],
    }

    service { "sendmail":
        ensure => stopped,
        enable => false,
        before => Service["smtpd"],
    }

    service { "smtpd":
        ensure => running,
        enable => true,
        start  => "/usr/sbin/smtpd",
    }

}


# Configure smtpd as mail server
#
# === Parameters
#
#   $maildir:
#       Directory in user home for INBOX.
#
#   $local:
#       Boolean for whether we accept mail for local recipients.
#       Defaults to true.
#
#   $gecos:
#       Boolean for whether to enable gecos aliases.
#       Defaults to false.
#
#   $domains:
#       Array of primary domains to accept mail for.
#
#   $virtual:
#       Array of virtual domains to accept mail for.
#
#   $ssl_key:
#       Source path of private key.
#
#   $ssl_cert:
#       Source path of certificate.
#
class smtpd::server(
    $maildir,
    $local=true,
    $gecos=false,
    $domains=undef,
    $virtual=undef,
    $ssl_key="${puppet_ssldir}/private_keys/${homename}.pem",
    $ssl_cert="${puppet_ssldir}/certs/${homename}.pem"
) inherits smtpd {

    include procmail

    procmail::rc { "00-default.rc":
        content => "MAILDIR=\$HOME/${maildir}\nDEFAULT=\$MAILDIR/INBOX\n",
    }

    $mda = "/usr/local/bin/procmail -Y -t -f %{sender}"

    File["/etc/mail/smtpd.conf.local"] {
        content => template("smtpd/server.conf.erb"),
    }

    file { [ "/root/${maildir}", "/etc/skel/${maildir}" ]:
        ensure => directory,
        mode   => "0700",
        owner  => "root",
        group  => "wheel",
        before => Service["smtpd"],
    }

    file { "/etc/mail/certs":
        ensure => directory,
        mode   => "0700",
        owner  => "root",
        group  => "wheel",
    }
    file { "/etc/mail/certs/server.key":
        ensure => present,
        mode   => "0600",
        owner  => "root",
        group  => "wheel",
        source => $ssl_key,
        notify => Service["smtpd"],
    }
    file { "/etc/mail/certs/server.crt":
        ensure => present,
        mode   => "0600",
        owner  => "root",
        group  => "wheel",
        source => $ssl_cert,
        notify => Service["smtpd"],
    }

    if $gecos == true {
        file { "/usr/local/sbin/generate-smtpd-gecos.sh":
            ensure => present,
            mode   => "0700",
            owner  => "root",
            group  => "wheel",
            source => "puppet:///modules/smtpd/generate-smtpd-gecos.sh",
        }
        exec { "/usr/local/sbin/generate-smtpd-gecos.sh":
            unless  => "/bin/test /etc/mail/gecos -nt /etc/passwd",
            require => File["/usr/local/sbin/generate-smtpd-gecos.sh"],
            notify  => Exec["makemap aliases"],
        }
    }

    file { "/etc/mail/aliases":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "wheel",
        source => [
            "puppet:///files/mail/aliases.${::homename}",
            "puppet:///files/mail/aliases",
        ],
    }
    exec { "makemap aliases":
        command     => $gecos ? {
            false => "makemap aliases",
            true  => "cat aliases gecos > aliases.gecos && makemap -o aliases.db aliases.gecos",
        },
        refreshonly => true,
        cwd         => "/etc/mail",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        subscribe   => File["/etc/mail/aliases"],
        before      => Service["smtpd"],
    }

    file { "/etc/mail/clients":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "wheel",
        source => [
            "puppet:///files/mail/clients.${::homename}",
            "puppet:///files/mail/clients",
            "puppet:///modules/smtpd/empty",
        ],
    }
    exec { "makemap -t set clients":
        refreshonly => true,
        cwd         => "/etc/mail",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        subscribe   => File["/etc/mail/clients"],
        before      => Service["smtpd"],
    }

    if $domains {
        smtpd::aliases { $domains:
            gecos     => $gecos,
            subscribe => $gecos ? {
                false => undef,
                true  => Exec["/usr/local/sbin/generate-smtpd-gecos.sh"],
            },
        }
    }

    if $virtual {
        smtpd::virtual { $virtual: }
    }

}


define smtpd::aliases($gecos) {

    file { "/etc/mail/aliases.${name}":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "wheel",
        source => [
            "puppet:///files/mail/aliases.${name}",
            "puppet:///files/mail/aliases.${::homename}",
            "puppet:///files/mail/aliases",
        ],
    }
    exec { "makemap aliases.${name}":
        command     => $gecos ? {
            false => "makemap aliases.${name}",
            true  => "cat aliases.${name} gecos > aliases.${name}.gecos && makemap -o aliases.${name}.db aliases.${name}.gecos",
        },
        refreshonly => true,
        cwd         => "/etc/mail",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        subscribe   => File["/etc/mail/aliases.${name}"],
        before      => Service["smtpd"],
    }

}


define smtpd::virtual() {

    file { "/etc/mail/virtual.${name}":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "wheel",
        source => [
            "puppet:///files/mail/virtual.${name}",
            "puppet:///files/mail/virtual.${::homename}",
            "puppet:///files/mail/virtual",
        ],
    }
    exec { "makemap virtual.${name}":
        refreshonly => true,
        cwd         => "/etc/mail",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        subscribe   => File["/etc/mail/virtual.${name}"],
        before      => Service["smtpd"],
    }

}
