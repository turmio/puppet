# Install logwatch.
#
# === Global variables
#
#   $logwatch_mailto:
#       Logwatch mail recipient.
#
#   $logwatch_level:
#       Logwatch detail level for report. Valid values are
#       from 0 to 10.
#
class logwatch {

    case $::kernel {
        "linux": {
            package { "logwatch":
                ensure => installed,
            }
        }
        default: {
            fail("logwatch not supported on ${::kernel}")
        }
    }

    file { "/etc/logwatch/conf/logwatch.conf":
        ensure  => present,
        content => template("logwatch/logwatch.conf.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["logwatch"],
    }

}
