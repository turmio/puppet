# Install tools for detected raid controllers.
#
class raid::tools {

    if $::raid {
        $controllers = split($::raid, ',')

        if "smartarray" in $controllers {
            include raid::tools::hpacucli
        }
        if "megaraid" in $controllers {
            include raid::tools::megacli
        }
    }

}


# Install HP SmartArray tools.
#
class raid::tools::hpacucli {

    case $::operatingsystem {
        "centos","redhat": {
            include yum::repo::hpspp
            package { "hpacucli":
                ensure  => installed,
                require => Yum::Repo["hpspp"],
            }
        }
        default: {
            fail("raid::tools::smartarray not supported in ${::operatingsystem}")
        }
    }

}


# Install MegaCli.
#
# Download MegaCli RPM from:
# http://www.lsi.com/support/Pages/Download-Results.aspx?keyword=MegaCli
#
# === Global variables
#
#   $raid_megacli_package:
#       Name of MegaCli package.
#
class raid::tools::megacli {

    if ! $raid_megacli_package {
        fail("Must define \$raid_megacli_package")
    }

    if ! ($::operatingsystem in ["CentOS", "RedHat"]) {
        fail("raid::tools::megacli not supported in ${::operatingsystem}")
    }

    file { "/usr/local/src/${raid_megacli_package}":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => "puppet:///files/packages/${raid_megacli_package}",
        before => Package["MegaCli"],
    }
    package { "MegaCli":
        ensure   => latest,
        provider => "rpm",
        source   => "/usr/local/src/${raid_megacli_package}",
    }

}
