#!/bin/bash
#
# pulseaudio:   Starts the pulseaudio system wide daemon.
#
# chkconfig: - 95 05
# description:  This is a daemon which handles passwd and group lookups \
#               for running programs and cache the results for the next \
#               query.  You should start this daemon if you use \
#               slow naming services like NIS, NIS+, LDAP, or hesiod.
# processname: /usr/bin/pulseaudio
#

# Source function library.
. /etc/init.d/functions

if [ -f /etc/sysconfig/pulseaudio ] ; then
        . /etc/sysconfig/pulseaudio
fi

RETVAL=0
prog=pulseaudio

start() {
    echo -n $"Starting $prog: "
    daemon pulseaudio --system --use-pid-file -n -F /etc/pulse/system.pa -D ${PULSEAUDIO_OPTIONS}
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && touch /var/lock/subsys/pulseaudio
    return $RETVAL
}

stop() {
    echo -n $"Stopping $prog: "
    killproc pulseaudio
    RETVAL=$?
    [ $RETVAL -eq 0 ] && rm -f /var/lock/subsys/pulseaudio
    echo
    return $RETVAL
}

restart() {
    stop
    start
}

# See how we were called.
case "$1" in
    start)
        start
        RETVAL=$?
        ;;
    stop)
        stop
        RETVAL=$?
        ;;
    status)
        status pulseaudio
        RETVAL=$?
        ;;
    restart)
        restart
        RETVAL=$?
        ;;
    try-restart | condrestart)
        [ -e /var/lock/subsys/pulseaudio ] && restart
        RETVAL=$?
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart}"
        RETVAL=1
        ;;
esac
exit $RETVAL
